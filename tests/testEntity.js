const assert = require('assert');

const mongoose = require('mongoose');
let config  = require('./config.json');

const mongooseHandler = require('mongoose-handler-nt');

const {tagEntityModel} = require("./TagEntity");

const entities_id = ["5b01a8ab8337ed5bfb21f7c6"];

const user = {id: "5b01a8ab8337ed5bfb21f7c6", entities_id: ["5b01a8ab8337ed5bfb21f7c6", "5b01a8ab8337ed5bfb21f7c7"]};
const user2 = {id: "5b01a8ab8337ed5bfb21f7c6", entities_id: ["5b01a8ab8337ed5bfb21f7c7"]};
const user3 = {id: "5b01a8ab8337ed5bfb21f7c6", entities_id: []};

let _id = null;

const nbAll = 10;

if (process.env.NODE_ENV === "test")
  config = config.DEV;
else
  config = config.CI;

describe("Start mongoose", async () =>
{
  it("Connection DB", async () =>
  {
    try
    {
      await connectDB();
      console.log("CONNECTED");
      await tagEntityModel.deleteMany({});
    }
    catch (e)
    {
      //console.error(e);
      process.exit(1);
    }
  });
});


describe('Create', () =>
{
  it("Should fail", () =>
  {
    tagEntityModel
    .createMeta([{label: "Test"}], user, entities_id)
    .then(data =>
    {
      assert.equal(data, null);
    })
    .catch(err =>
    {
      assert.notEqual(err, null);
    });
  });

  it("Simple insertion", (done) =>
  {
    tagEntityModel
    .createMeta({label : "Test"}, user, entities_id)
    .then(async (data) =>
    {
      _id = data._id;
      //console.log(_id);

      for (let i = 1; i < 10; i++)
        await tagEntityModel.createMeta({label: "Test"+i}, user, entities_id);

      done();
    })
    .catch(err =>
    {
      assert.equal(err, null);
    });
  });

  it ("Verify insertion", (done) =>
  {
    tagEntityModel
    .find({})
    .exec()
    .then(data =>
    {
      //console.log(data);
      assert.equal(data.length, nbAll);
      done();
    })
    .catch(err =>
    {
      console.log(err);
    });
  })

});


describe('Find', () =>
{

  it ("FindAll & Count", (done) =>
  {
    tagEntityModel
    .findMeta({}, user)
    .populate("user_id")
    .then(async (data) =>
    {
      let b = await tagEntityModel.countMeta({}, user);
      assert.equal(data.length, b);
      assert.equal(b, nbAll);
      done();
    })
    .catch(err =>
    {
      console.log(err);
      assert.equal(err, null);
    });
  });

  it ("FindAll & Count - User3", (done) =>
  {
    tagEntityModel
    .findMeta({}, user3)
    .populate("user_id")
    .then(async (data) =>
    {
      let b = await tagEntityModel.countMeta({}, user3);
      assert.equal(data.length, b);
      assert.equal(b, nbAll);
      done();
    })
    .catch(err =>
    {
      console.log(err);
      assert.equal(err, null);
    });
  });

  it ("FindAll with another entity id", (done) =>
  {
    tagEntityModel
    .findMeta({}, user2)
    .populate("user_id")
    .then(async (data) =>
    {
      assert.equal(data.length, 0);
      done();
    })
    .catch(err =>
    {
      assert.equal(err, null);
    });
  });


  it ("FindOne", (done) =>
  {
    tagEntityModel
    .findOneMeta({label: "Test"}, user)
    .select('label')
    .populate("user_id")
    .then((data) =>
    {
      //console.log(data);
      assert.notEqual(data, null);
      assert.equal(data.label, "Test");
      done();
    })
    .catch(err =>
    {
      return false;
    });
  });

  it ("FindOne - Projection", (done) =>
  {
    tagEntityModel
    .findOneMeta({label: "Test"}, user)
    .projection()
    .populate("user_id")
    .then((data) =>
    {
      assert.notEqual(data, null);
      assert.equal(data.label, "Test");
      assert.equal(data.hasOwnProperty("meta"), false);
      assert.equal(data.hasOwnProperty("__v"), false);
      done();
    })
    .catch(err =>
    {
      return false;
    });
  });

  it ("FindOne - Projection2", (done) =>
  {
    tagEntityModel
    .findOneMeta({label: "Test"}, user)
    .projection({label: 1})
    .populate("user_id")
    .then((data) =>
    {
      assert.notEqual(data, null);
      assert.equal(data.label, "Test");
      done();
    })
    .catch(err =>
    {
      //console.log(err);
      return false;
    });
  });

  it ("FindOne - User3", (done) =>
  {
    tagEntityModel
    .findOneMeta({label: "Test"}, user3)
    .select('label')
    .populate("user_id")
    .then((data) =>
    {
      //console.log(data);
      assert.notEqual(data, null);
      assert.equal(data.label, "Test");
      done();
    })
    .catch(err =>
    {
      return false;
    });
  });

  it('FindById', async () =>
  {
    tagEntityModel
    .findByIdMeta(_id, user)
    .select('meta')
    .then(data =>
    {
      //console.log(data);
      assert.notEqual(data, null);
      assert.notEqual(data.meta, null);
    })
    .catch(err =>
    {
      return false;
    });
  });

  it('FindById with another entity id', async () =>
  {
    tagEntityModel
    .findByIdMeta(_id, user2)
    .select('meta')
    .then(data =>
    {
      assert.equal(data, null);
    })
    .catch(err =>
    {
      return false;
    });
  });
});



describe('Update', function()
{

  it('FindOneAndUpdate', async () =>
  {

    let a = await tagEntityModel.findOneAndUpdateMeta({_id: _id}, {label: "Custom"}, user);

    let b = await tagEntityModel.findByIdMeta(_id, user);

    assert.equal(a.label, "Custom");
    assert.equal(b.label, "Custom");
    assert.equal(a.meta.updated.length, 1);
  });

  it('FindByIdAndUpdate', async () =>
  {
    const newLabel = "CustomFindByIdUpdate";

    let a = await tagEntityModel.findByIdAndUpdateMeta(_id, {label: newLabel}, user);
    let b = await tagEntityModel.findByIdMeta(_id, user);

    assert.equal(a.label, newLabel);
    assert.equal(b.label, newLabel);
    assert.equal(a.meta.updated.length, 2);
  });

  it('Update', async () =>
  {
    let a = await tagEntityModel.findOneMeta({label: "Test1"}, user);

    if (a)
    {
      let b = await a.updateMeta({label: "CustomUpdate"}, user);
      let c = await tagEntityModel.findOneMeta({label: "CustomUpdate"}, user);

      assert.notEqual(c, null);
    }
  });

});


describe('Delete', function ()
{

  it('findOneAndRemove', async () =>
  {
    try
    {
      let a = await tagEntityModel.findOneAndRemoveMeta({label: "Test2"}, user);
      let b = await tagEntityModel.findOneAndRemoveMeta({label: "Test2"}, user);
    }
    catch (e)
    {
      assert.notEqual(e, null);
    }

  });

  it('findOneAndDelete', (done) =>
  {
    tagEntityModel
    .findOneAndRemoveMeta({label: "Test3"}, user)
    .then(async (data) =>
    {
      let b = await tagEntityModel.findOneMeta({label: "Test3"}, user);
      assert.equal(b, null);
      done();
    })
    .catch(err =>
    {
      assert.equal(err, null);
    });
  });


  it('findByIdAndRemove', async () =>
  {
    let a = await tagEntityModel.findOneMeta({label: "Test4"}, user);
    assert.notEqual(a, null);

    tagEntityModel
    .findByIdAndRemoveMeta(a._id, user)
    .then(async (data) =>
    {
      try
      {
        let b = await tagEntityModel.findOneMeta({label: "Test4"}, user);
        assert.equal(b, null);
      }
      catch (e)
      {
        console.log(e);
        assert.equal(e, null);
      }
    })
    .catch(err =>
    {
      assert.equal(err, null);
    });

  });

  it('findByIdAndDelete', (done) =>
  {
    tagEntityModel
    .findOneMeta({label: "Test5"}, user)
    .then(async (data) =>
    {
      let c = await tagEntityModel.findByIdAndDeleteMeta(data._id, user);
      let b = await tagEntityModel.findOneMeta({label: "Test5"}, user);
      assert.equal(b, null);
      done();
    })
    .catch(err =>
    {
      assert.equal(err, null);
    })
  });

  it('DeleteOne', (done) =>
  {
    tagEntityModel
    .findOneMeta({label: "Test6"}, user)
    .then(async (data) =>
    {
      tagEntityModel
      .deleteOneMeta({_id: data._id}, user)
      .then(async data2 =>
      {
        let b = await tagEntityModel.findOneMeta({label: "Test6"}, user);
        assert.equal(b, null);
        done();
      })
      .catch(err =>
      {
        assert.equal(err, null);
      });

    })
    .catch(err =>
    {
      assert.equal(err, null);
    });
  });


  it('DeleteMany', async () =>
  {
    let a = await tagEntityModel.findOneMeta({label: "Test8"}, user);
    if (a)
    {
      let c = await tagEntityModel.deleteMeta({_id: a._id}, user);
      let b = await tagEntityModel.findOneMeta({label: "Test8"}, user);

      assert.equal(b, null);
    }
  });


  it('No soft delete : findByIdAndDelete', async () =>
  {
    tagEntityModel
    .findOneMeta({label: "Test7"}, user)
    .then(async (data) =>
    {
      let c = await tagEntityModel.findByIdAndDeleteMeta(data._id, user, false);
      let b = await tagEntityModel.findOneMeta({label: "Test7"}, user);

      assert.equal(b, null);
    })
    .catch(err =>
    {
      assert.equal(err, null);
    });
  });

});


describe('Find With deleted', () =>
{
  it('FindDeleted', async function ()
  {
    let a = await tagEntityModel.findWithDeleted({}, user);
    //console.log(a);
    assert.equal(a.length, nbAll-1);
  });

  it('FindOneDeleted', async function ()
  {
    let a = await tagEntityModel.findOneWithDeleted({label: "Test2"}, user);

    assert.notEqual(a, null);
  });

  it("CountDeleted", async function ()
  {
    let a = await tagEntityModel.countWithDeleted({}, user);
    assert.equal(a, nbAll-1);
  });

});

/*
describe('Search', () =>
{
  it('Simple search', async () =>
  {
    let a = await tagEntityModel.simpleSearch("test", null, user);
    let b = await tagEntityModel.simpleSearch("DEFAULT", ["label", "label2"], user);

    assert.equal(a.length, 1);
    assert.equal(b.length, 3)
  })
});
*/
describe('Pagination', function ()
{
  it("Pagination", async () =>
  {
    let a =  await tagEntityModel.simpleSearch("DEFAULT", ["label2"], user).pagination(0, 1);
    assert.equal(a.length, 1);
  });
});


async function connectDB()
{
  return mongooseHandler.connect(config.MONGODB);
}
