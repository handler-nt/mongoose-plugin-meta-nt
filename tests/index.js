const assert = require('assert');
const mongoose = require('mongoose');
const config = (process.env.NODE_ENV === "test") ? require('./config.json').DEV : require('./config.json').CI;
const mongooseHandler = require('mongoose-handler-nt');

mongooseHandler.connect(config.MONGODB);

const {tagModel} = require("./Tag");


const user = {id: "5b01a8ab8337ed5bfb21f7c6"};

let _id = null;

const nbAll = 10;


describe("Start mongoose", async () =>
{
  it("Connection DB", async () =>
  {
    try
    {
      //await connectDB();
      console.log("CONNECTED");
      await tagModel.remove({});
    }
    catch (e)
    {
      process.exit(1);
    }
  });
});


describe('Create', () =>
{
  it("Should fail", () =>
  {
    tagModel
    .createMeta([{label: "Test"}], user)
    .then(data =>
    {
      assert.equal(data, null);
    })
    .catch(err =>
    {
      assert.notEqual(err, null);
    });
  });

  it("Simple insertion", (done) =>
  {
    tagModel
    .createMeta({label : "Test"}, user)
    .then(async (data) =>
    {
      _id = data._id;
      console.log(_id);

      for (let i = 1; i < 10; i++)
        await tagModel.createMeta({label: "Test"+i}, user);

      done();
    })
    .catch(err =>
    {
      console.log(err);
      assert.equal(err, null);
    });
  });

  it ("Verify insertion", (done) =>
  {
    tagModel
    .find({})
    .exec()
    .then(data =>
    {
      assert.equal(data.length, nbAll);
      done();
    })
    .catch(err =>
    {
      console.log(err);
    });
  })

  /*
  it ("Multiple insertions", async () =>
  {
    tagModel.createMany([{label: "TESTA"}, {label: "TESTB"}], user, (err, data) =>
    {
      if (err)
        return err;
      assert.notEqual(data, null);
      //assert.equal(data.length, 2);
    })
  });
  */
});


describe('Find', () =>
{

  it ("FindAll & Count", (done) =>
  {
    tagModel
    .findMeta()
    .populate("user_id")
    .then(async (data) =>
    {
      let b = await tagModel.countMeta();
      assert.equal(data.length, b);
      assert.equal(b, nbAll);
      done();
    })
    .catch(err =>
    {
      console.log(err);
      assert.equal(err, null);
    });
  });


  it ("FindOne", (done) =>
  {
    tagModel
    .findOneMeta({label: "Test"}, user)
    .select('label')
    .populate("user_id")
    .then((data) =>
    {
      console.log(data);
      assert.notEqual(data, null);
      assert.equal(data.label, "Test");
      done();
    })
    .catch(err =>
    {
      return false;
    });
  });


  it('FindById', async () =>
  {
    tagModel
    .findByIdMeta(_id, user)
    .select('meta')
    .then(data =>
    {
      console.log(data);
      assert.notEqual(data, null);
      assert.notEqual(data.meta, null);
    })
    .catch(err =>
    {
      return false;
    });
  });

});



describe('Update', function()
{

  it('FindOneAndUpdate', async () =>
  {

    let a = await tagModel.findOneAndUpdateMeta({_id: _id}, {label: "Custom"}, user);

    let b = await tagModel.findByIdMeta(_id, user);

    assert.equal(a.label, "Custom");
    assert.equal(b.label, "Custom");
    assert.equal(a.meta.updated.length, 1);
  });

  it('FindByIdAndUpdate', async () =>
  {
    const newLabel = "CustomFindByIdUpdate";

    let a = await tagModel.findByIdAndUpdateMeta(_id, {label: newLabel}, user);
    let b = await tagModel.findByIdMeta(_id, user);

    assert.equal(a.label, newLabel);
    assert.equal(b.label, newLabel);
    assert.equal(a.meta.updated.length, 2);
  });

  it('Update', async () =>
  {
    let a = await tagModel.findOneMeta({label: "Test1"});

    if (a)
    {
      let b = await a.updateMeta({label: "CustomUpdate"}, user);
      let c = await tagModel.findOneMeta({label: "CustomUpdate"});

      assert.notEqual(c, null);
    }
  });

});


describe('Delete', function ()
{

  it('findOneAndRemove', async () =>
  {
    try
    {
      let a = await tagModel.findOneAndRemoveMeta({label: "Test2"}, user);
      let b = await tagModel.findOneAndRemoveMeta({label: "Test2"}, user);
    }
    catch (e)
    {
      assert.notEqual(e, null);
    }

  });

  it('findOneAndDelete', (done) =>
  {
    tagModel
    .findOneAndRemoveMeta({label: "Test3"}, user)
    .then(async (data) =>
    {
      let b = await tagModel.findOneMeta({label: "Test3"}, user);
      assert.equal(b, null);
      done();
    })
    .catch(err =>
    {
      assert.equal(err, null);
    });
  });


  it('findByIdAndRemove', async () =>
  {
    let a = await tagModel.findOneMeta({label: "Test4"}, user);
    assert.notEqual(a, null);

    tagModel
    .findByIdAndRemoveMeta(a._id, user)
    .then(async (data) =>
    {
      try
      {
        let b = await tagModel.findOneMeta({label: "Test4"}, user);
        assert.equal(b, null);
      }
      catch (e)
      {
        console.log(e);
        assert.equal(e, null);
      }
    })
    .catch(err =>
    {
      assert.equal(err, null);
    });

  });

  it('findByIdAndDelete', (done) =>
  {
    tagModel
    .findOneMeta({label: "Test5"}, user)
    .then(async (data) =>
    {
      let c = await tagModel.findByIdAndDeleteMeta(data._id, user);
      let b = await tagModel.findOneMeta({label: "Test5"}, user);
      assert.equal(b, null);
      done();
    })
    .catch(err =>
    {
      assert.equal(err, null);
    })
  });

  it('DeleteOne', (done) =>
  {
    tagModel
    .findOneMeta({label: "Test6"}, user)
    .then(async (data) =>
    {
      tagModel
      .deleteOneMeta({_id: data._id}, user)
      .then(async data2 =>
      {
        let b = await tagModel.findOneMeta({label: "Test6"}, user);
        assert.equal(b, null);
        done();
      })
      .catch(err =>
      {
        assert.equal(err, null);
      });

    })
    .catch(err =>
    {
      assert.equal(err, null);
    });
  });


  it('DeleteMany', async () =>
  {
    let a = await tagModel.findOneMeta({label: "Test8"}, user);
    if (a)
    {
      let c = await tagModel.deleteMeta({_id: a._id}, user);
      let b = await tagModel.findOneMeta({label: "Test8"}, user);

      assert.equal(b, null);
    }
  });


  it('No soft delete : findByIdAndDelete', async () =>
  {
    tagModel
    .findOneMeta({label: "Test7"}, user)
    .then(async (data) =>
    {
      let c = await tagModel.findByIdAndDeleteMeta(data._id, user, false);
      let b = await tagModel.findOneMeta({label: "Test7"}, user);

      assert.equal(b, null);
    })
    .catch(err =>
    {
      assert.equal(err, null);
    });
  });

});


describe('Find With deleted', () =>
{
  it('FindDeleted', async function ()
  {
    let a = await tagModel.findWithDeleted();
    console.log(a);
    assert.equal(a.length, 9);
  });

  it('FindOneDeleted', async function ()
  {
    let a = await tagModel.findOneWithDeleted({label: "Test2"});

    assert.notEqual(a, null);
  });

  it("CountDeleted", async function ()
  {
    let a = await tagModel.countWithDeleted();
    assert.equal(a, 9);
  });

});


describe('Search', () =>
{
  it('Simple search', async () =>
  {
    let a = await tagModel.simpleSearch("test");
    let b = await tagModel.simpleSearch("DEFAULT", ["label", "label2"]);

    assert.equal(a.length, 1);
    assert.equal(b.length, 3)
  })
});

describe('Pagination', function ()
{
  it("Pagination", async () =>
  {
    let a =  await tagModel.simpleSearch("DEFAULT", ["label2"]).pagination(0, 1);
    assert.equal(a.length, 1);
  });
});

/*
async function connectDB()
{
  return await mongooseHandler.connect(config.MONGODB);
}
*/
