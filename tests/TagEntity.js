const mongoose = require('mongoose');
const plugin = require('../index');
const {User} = require('./User');

const tagSchema = new mongoose.Schema(
{
  label: {type: String, required: true},
  label2: {type: String, default: "DEFAULT"},
  user_id: {type: mongoose.Schema.Types.ObjectId, ref: "User", default: null}
},
{
  collection: "tags"
});

tagSchema.plugin(plugin.metaManager, {entity: true, searchFields: ["label"]});

const tagEntityModel = mongoose.model('Tag', tagSchema);

module.exports = {tagEntityModel};