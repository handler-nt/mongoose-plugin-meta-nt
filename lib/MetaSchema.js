const mongoose = require('mongoose');


const metaSchema = new mongoose.Schema(
{
  created:
  {
    date: {type: Date, default: null},
    user: {type: mongoose.Schema.ObjectId, default: null}
  },
  updated:
  [
    {
      date: {type: Date, default: null},
      user: {type: mongoose.Schema.ObjectId, default: null}
    }
  ],
  deleted:
  {
    date: {type: Date, default: null},
    user: {type: mongoose.Schema.ObjectId, default: null}
  },
  entities_id: {type: [{type: mongoose.Schema.Types.ObjectId, ref: "Entity"}], default: []}
});

module.exports =
{
  metaSchema: metaSchema
};
