const mongoose = require('mongoose');
const Model = mongoose.Model;

const errorHandler = require('error-handler-nt');

const metaSchema = require('./lib/MetaSchema');

const namePlugin = "MetaManager (Mongoose Plugin)";

const cloneDeep = require('clone-deep');

function metaManager(schema, options)
{
  schema.add({meta: getMetaSchema()});

  /**
   * Find function
   * @param {object} obj
   * @param {object} user
   * @param arrPopulate
   * @param {boolean} entity_filter
   * @return {Query}
   */
  schema.statics["findMeta"] = function(obj = {}, user = {}, arrPopulate = [], entity_filter = null)
  {
    obj['meta.deleted.date'] = null;

    let q = this.find(obj);


    if ((options.entity && entity_filter === null && user.entities_id.length > 0) || entity_filter)
    {
      q.or([
        {
          'meta.entities_id': []
        },
        {
          'meta.entities_id': {$in: user.entities_id}
        }]);
    }

    for (let i = 0; i < arrPopulate.length; i++)
    {
      q.populate(arrPopulate[i])
    }

    return q;
  };

  /**
   * FindOne
   * @param {object} obj
   * @param {object} user
   * @param arrPopulate
   * @param {boolean} entity_filter
   * @return {Query}
   */
  schema.statics["findOneMeta"] = function(obj, user = {}, arrPopulate = [], entity_filter = null)
  {
    obj['meta.deleted.date'] = null;

    let q = this.findOne(obj);

    if ((options.entity && entity_filter === null && user.entities_id.length > 0) || entity_filter)
    {
      q.or(
      [
        {
          'meta.entities_id': []
        },
        {
          'meta.entities_id': {$in: user.entities_id}
        }]);
    }

    for (let i = 0; i < arrPopulate.length; i++)
    {
      q.populate(arrPopulate[i])
    }

    return q;
  };

  /**
   * Find with deleted
   * @param obj
   * @param user
   * @param arrPopulate
   * @param entity_filter
   * @return {*}
   */
  schema.statics["findWithDeleted"] = function (obj = {}, user = {}, arrPopulate = [], entity_filter = null)
  {
    let q = this.find(obj);

    if ((options.entity && entity_filter === null && user.entities_id.length > 0) || entity_filter)
    {
      q.or(
      [
        {
          'meta.entities_id': []
        },
        {
          'meta.entities_id': {$in: user.entities_id}
        }
      ])
    }

    for (let i = 0; i < arrPopulate.length; i++)
    {
      q.populate(arrPopulate[i])
    }

    return q;
  };


  /**
   * Find one with deleted
   * @param obj
   * @param user
   * @param arrPopulate
   * @param entity_filter
   * @return {*}
   */
  schema.statics["findOneWithDeleted"] = function(obj, user = {}, arrPopulate = [], entity_filter = null)
  {
    let q = this.find(obj);

    if ((options.entity && entity_filter === null && user.entities_id.length > 0) || entity_filter)
    {
      q.or(
      [
        {
          'meta.entities_id': []
        },
        {
          'meta.entities_id': {$in: user.entities_id}
        }
      ])
    }

    for (let i = 0; i < arrPopulate.length; i++)
    {
      q.populate(arrPopulate[i])
    }


    return q;
  };

  /**
   * Count with deleted
   * @param obj
   * @param user
   * @param entity_filter
   * @return {*}
   */
  schema.statics["countWithDeleted"] = function(obj = {}, user = {}, entity_filter = null)
  {

    let q = this.countDocuments(obj);

    if ((options.entity && entity_filter === null && user.entities_id.length > 0) || entity_filter)
    {
      q.or(
      [
        {
          'meta.entities_id': []
        },
        {
          'meta.entities_id': {$in: user.entities_id}
        }
      ])
    }

    return q;
  };


  /**
   * Find One and update
   * @param obj
   * @param newObj
   * @param user
   * @param newEntitiesID
   * @param entity_filter
   * @return {Promise<any>}
   */
  schema.statics["findOneAndUpdateMeta"] = function(obj, newObj, user, newEntitiesID = null, entity_filter = null)
  {
    return new Promise((resolve, reject) =>
    {
      this
      .findOneMeta(obj, user, [], entity_filter)
      .then(async (data) =>
      {
        return resolve(data.updateMeta(newObj, user, newEntitiesID));
      })
      .catch(err =>
      {
        return reject(err);
      });
    });
  };

  /**
   * Find by Id
   * @param id
   * @param user
   * @param arrPopulate
   * @param entity_filter
   * @return {Query}
   */
  schema.statics["findByIdMeta"] = function(id, user = {}, arrPopulate = [], entity_filter = null)
  {
    return this.findOneMeta({_id: mongoose.Types.ObjectId(id)}, user, arrPopulate, entity_filter);
  };

  /**
   * Find by Id and Update
   * @param id
   * @param obj
   * @param user
   * @param newEntities
   * @param entity_filter
   * @return {Promise<any>}
   */
  schema.statics["findByIdAndUpdateMeta"] = function(id, obj, user, newEntities = null, entity_filter = null)
  {
    return this.findOneAndUpdateMeta({_id: mongoose.Types.ObjectId(id)}, obj, user, newEntities, entity_filter);
  };

  /**
   * Find by Id and Remove
   * @param id
   * @param user
   * @param isSoftDelete
   * @param entity_filter
   * @return {Promise<any>}
   */
  schema.statics["findByIdAndRemoveMeta"] = function(id, user, isSoftDelete = true, entity_filter = null)
  {
    return this.deleteOneMeta({_id: mongoose.Types.ObjectId(id)}, user, isSoftDelete, entity_filter);
  };

  /**
   * Find by Id and Delete
   * @param id
   * @param user
   * @param isSoftDelete
   * @param entity_filter
   * @return {Promise<any>}
   */
  schema.statics["findByIdAndDeleteMeta"] = function(id, user, isSoftDelete = true, entity_filter = null)
  {
    return this.deleteOneMeta({_id: mongoose.Types.ObjectId(id)}, user, isSoftDelete, entity_filter);
  };

  /**
   * Find One and Delete
   * @param obj
   * @param user
   * @param isSoftDelete
   * @param entity_filter
   * @return {Promise<any>}
   */
  schema.statics["findOneAndDeleteMeta"] = function(obj, user, isSoftDelete = true, entity_filter = null)
  {
    return this.deleteOneMeta(obj, user, isSoftDelete, entity_filter);
  };

  /**
   * Find One and Remove
   * @param obj
   * @param user
   * @param isSoftDelete
   * @param entity_filter
   * @return {Promise<any>}
   */
  schema.statics["findOneAndRemoveMeta"] = function(obj, user, isSoftDelete = true, entity_filter = null)
  {
    return this.deleteOneMeta(obj, user, isSoftDelete, entity_filter);
  };


  /**
   * Count
   * @param {object} obj
   * @param {object} user
   * @param {boolean} entity_filter
   * @return {Query}
   */
  schema.statics["countMeta"] = function(obj = {}, user = {}, entity_filter = null)
  {
    obj["meta.deleted.date"] = null;

    let q = this.countDocuments(obj);

    if ((options.entity && entity_filter === null && user.entities_id.length > 0) || entity_filter)
    {
      q.or([
        {
          'meta.entities_id': []
        },
        {
          'meta.entities_id': {$in: user.entities_id}
        }]);
    }

    return q;
  };

  /**
   * Create
   * @param {object} obj
   * @param {object} user
   * @param {array} entities_id
   * @return {Promise}
   */
  schema.statics["createMeta"] = function(obj, user, entities_id = null)
  {
    return new Promise((resolve, reject) =>
    {
      if (Array.isArray(obj))
        return reject(errorHandler.create(500, "Create function needs an object", "create - " + namePlugin));

      let el = new this(obj);


      el['meta']['created']['date'] = new Date();
      el['meta']['created']['user'] = (user) ? user.id : null;

      if (entities_id !== null)
        el['meta']['entities_id'] = entities_id;
      else if (obj.hasOwnProperty("entities"))
        el['meta']['entities_id'] = obj.entities;
      else
        el['meta']['entities_id'] = [];


      return resolve(el.save());
    });
  };


  /**
   * Delete One
   * @param obj
   * @param user
   * @param isSoftDelete
   * @param entity_filter
   * @return {Promise<any>}
   */
  schema.statics["deleteOneMeta"] =  function(obj, user, isSoftDelete = true, entity_filter = null)
  {
    return new Promise((resolve, reject) =>
    {
      if (isSoftDelete)
      {
        if (!user)
          user = {};

        this
        .findOneMeta(obj, user, [], entity_filter)
        .then(data =>
        {
          if (!data)
            return reject(errorHandler.create(404));

          data.meta.deleted.date = new Date();
          data.meta.deleted.user = (user) ? user.id : null;

          return resolve(data.save());
        })
        .catch(err =>
        {
          return reject(err);
        });
      }
      else
      {
        return resolve(this.deleteOne(obj));
      }
    });
  };



  /**
   * Delete
   * @param obj
   * @param user
   * @param isSoftDelete
   * @param entity_filter
   * @return {Promise<any>}
   */
  schema.statics["deleteMeta"] = function(obj, user, isSoftDelete = true, entity_filter = null)
  {
    return new Promise((resolve, reject) =>
    {
      if (isSoftDelete)
      {
        if(!user)
          user = {};

        this
        .findMeta(obj, user, [], entity_filter)
        .then(async (data) =>
        {
          for (let i = 0; i < data.length; i++)
          {
            data[i].meta.deleted.date = new Date();

            data[i].meta.deleted.user = (user) ? user.id : null;
            await data[i].save();
          }

          return resolve(data);
        })
        .catch(err =>
        {
          return reject(err);
        });
      }
      else
        return resolve(this.deleteMany(obj));
    });
  };


  /*** WITH DELETE ***/


  schema.statics["simpleSearch"] = function(query, fields = [], user = {}, arrPopulate = [], entity_filter = null)
  {
    let arr = [];

    if (fields === null || fields.length === 0)
    {
      if (typeof options.searchFields !== 'undefined' && Array.isArray(options.searchFields))
      {
        options.searchFields.forEach((field) =>
        {
          let tmp = {};
          tmp[field] = new RegExp('.*' + query.toLowerCase() + '.*', "i");
          arr.push(tmp);
        });
      }
    }
    else
    {
      fields.forEach((field) =>
      {
        let tmp = {};
        tmp[field] = new RegExp('.*' + query.toLowerCase() + '.*', "i");
        arr.push(tmp);
      });
    }

    //console.log(arr);
    return this.findMeta({$or: arr}, user, arrPopulate, entity_filter);
  };


  /*************************************************** METHODS ***************************************************/

  /**
   * Update
   * @param newObj
   * @param user
   * @param newEntities
   * @return {Promise<any>}
   */
  schema.methods["updateMeta"] = function(newObj, user, newEntities = null)
  {
    return new Promise((resolve, reject) =>
    {
      if (this.meta)
      {
        // Set meta
        newObj['meta'] = cloneDeep(this.meta);
        Object.assign(this, newObj);

        const sizeUpdatedHistory = (options.sizeUpdatedHistory) ? options.sizeUpdatedHistory : 10;

        if (sizeUpdatedHistory === -1)
          this.meta.updated.unshift({date: new Date(), user: (user) ? user.id : null});
        else
          this.meta.updated = [{date: new Date(), user: (user) ? user.id : null}, ...this.meta.updated.slice(0, sizeUpdatedHistory)];

        if (newEntities !== null)
          this.meta.entities_id = newEntities;
        else if (newObj["entities"])
          this.meta.entities_id = newObj.entities;
        else if (newObj.meta.entities_id)
          this.meta.entities_id = newObj.meta.entities_id;

        return resolve(this.save());
      }
      else
        return reject(errorHandler.create(500));
    });
  };


  /**
   * QUERIES
   */
  schema.query["pagination"] = function(offset, limit)
  {
    return this.skip(parseInt(offset)).limit(parseInt(limit));
  };

  schema.query["projection"] = function(selection = null, showMeta = false, showVersion = false)
  {
    let obj = (selection) ? {...selection} : {};

    let isInclusion = false;

    for (let tmp in obj)
    {
      if (obj[tmp] === 1)
      {
        isInclusion = true;
        break;
      }
    }

    if (!isInclusion && !showMeta)
    {
      obj['meta.updated'] = 0;
      obj['meta.created'] = 0;
      obj['meta.deleted'] = 0;
    }
    if (!isInclusion && !showVersion)
      obj["__v"] = 0;

    return this.select(obj);
  }


}

function getMetaSchema()
{
  return metaSchema['metaSchema'].obj;
}

module.exports =
{
  metaManager
};
