# Mongoose Plugin Meta

This plugin enables soft deletion of documents, history of updates and date of creation in MongoDB.<br>
<b>It creates new functions that act like middlewares.</b><br>
At any time, it is possible to use the provided functions by Mongoose.

## Installation

```bash
$ npm install mongoose-plugin-meta-nt --save
```

## Documentation

### Example with TagModel
- Tag.js
````javascript
const mongoose = require('mongoose');
const metaPlugin = require('mongoose-plugin-meta-nt');

const tagSchema = new mongoose.Schema(
{
  label: {type: String, required: true}
},
{
  collection: "tags"
});

// Add plugin to the schema
tagSchema.plugin(metaPlugin.metaManager, {entity: false});

const Tag = mongoose.model('Tag', tagSchema);
````

### Statics

````javascript

createMeta(obj, user, entities_id = []);

findMeta(obj = {}, user = {}, arrPopulate = [], entity_filter = null);
findOneMeta(obj, user = {}, arrPopulate = [], entity_filter = null);
findByIdMeta(id, user = {}, arrPopulate = [], entity_filter = null);
countMeta(obj = {}, user = {}, entity_filter = null);


findOneAndUpdateMeta(obj, newObj, user, entity_filter = null);
findByIdAndUpdateMeta(id, obj, user, entity_filter = null);


findOneAndDeleteMeta(obj, user, isSoftDelete = true, entity_filter = null);
findOneAndRemoveMeta(obj, user, isSoftDelete = true, entity_filter = null);
findByIdAndRemoveMeta(id, user, isSoftDelete = true, entity_filter = null);
findByIdAndDeleteMeta(id, user, isSoftDelete = true, entity_filter = null);
deleteOneMeta(obj, user, isSoftDelete = true, entity_filter = null);
deleteMeta(obj, user, isSoftDelete = true, entity_filter = null);

// Get all documents
findWithDeleted(obj = {}, user = {}, arrPopulate = [], entity_filter = null);
findOneWithDeleted(obj, user = {}, arrPopulate = [], entity_filter = null);
countWithDeleted(obj = {}, user = {}, entity_filter = null);


// Search

// Filter the given fields with the query value.
simpleSearch(query, fields, user = {}, entity_filter = null); //  (NOT WORKING FOR NOW)


````

### Methods

```javascript
updateMeta(newObj, user);
```

### Queries
```` javascript
pagination(offset, limit);
projection(obj = null, showMeta = false, showVersion = false)
````

### Usage

````javascript

const user =
{
  id: "5b01a8ab8337ed5bfb21f7c6"
};

const id = "5b01a8ab8337ed5bfb21f1a1";

// Find - Promise
let data  = await Tag.findMeta({label: "Test"});
let data = await Tag.findOneMeta({label: "Test"});
let data = await Tag.findByIdMeta(id);

// Find - Projection
let data = await Tag.findOneMeta({label: "Test"}).projection(); // Will unselect 'meta' and '__v' fields
let data = await Tag.findOneMeta({label: "Test"}).projection({label2: 0}); // Will unselect 'label2', 'meta' and '__v' fields
let data = await Tag.findOneMeta({label: "Test"}).projection({label2: 1}); // Will select 'label2' and disabled others (MongoDB behavior) 

// Count - Promise
let data = await Tag.countMeta({label: "Test"});

// Create - Promise
let data = Tag.createMeta({label : "Test"}, user, entity_id);

// Update - Promise
let data = await Tag.findOneAndUpdateMeta({_id: id}, {label: "Custom"}, user);
let data = await Tag.findByIdAndUpdateMeta(id, {label: "CustomFindByIdUpdate"}, user);
let data = await Tag.updateMeta({label: "CustomUpdate"}, user);

// Delete - Promise
let data = await Tag.findOneAndRemoveMeta({label: "Test2"}, user);
let data = await Tag.findOneAndRemoveMeta({label: "Test3"}, user);
let data = await Tag.findByIdAndRemoveMeta(id, user);
let data = await Tag.findByIdAndDeleteMeta(id, user);
let data = await Tag.deleteOneMeta({}, user);
let data = await Tag.deleteMeta({}, user);
// Set to true for soft deletion. It is possible to set to false if you want real deletion 


// Find deleted documents - Promise
let data = await Tag.findDeleted({label: "Test"});
let data = await Tag.findOneDeleted({label: "Test2"});
let data = await Tag.countDeleted({label: "Test"});
````


### Produced document

````json
{ 
  "_id": "5b1995a5a87a731a94aa0abe",
  "label": "Test",
  "meta": 
  {
    "entities_id": [],
    "created":
    {
      "date": "2018-06-07T20:29:25.397Z",
      "user": "5b01a8ab8337ed5bfb21f7c6"
    },
    "deleted":
    {
      "date": "2018-06-07T20:38:06.762Z",
      "user": "5b01a8ab8337ed5bfb21f7c6"
    },
    "updated":
    [
      {
    	  "date": "2018-06-07T20:30:50.370Z",
          "user": "5b01a8ab8337ed5bfb21f7c6",
          "_id": "5b199906252b8d1ef4def526"
      }
    ]
  }
}

````
The _id is created in every items of the array. 

## Entity filtering
It is possible to enable the entity_id filtering :

- Set to <b>true</b> in the Model : <code>tagSchema.plugin(metaPlugin.metaManager, {entity: true});</code>
- If for any reason, the entity property is set to false in the model definition, it is possible to force the entity filtering by adding the parameter <b>true</b> in all functions.
- Set a value to the property <b>entity_id</b> in your user object.
- Put the <b>user object</b> in parameter of <b>all functions</b> :


````javascript

const entities_id = ["5b01a8ab8337ed5bfb21f7c6"];

const user =
{
  id: "5b01a8ab8337ed5bfb21f7c6",
  entities_id: ["5b01a8ab8337ed5bfb21f7c2", "5b01a8ab8337ed5bfb21f7c6"]
};

const id = "5b01a8ab8337ed5bfb21f1a1";

// Find - Promise
let data  = await Tag.findMeta({label: "Test"}, user);
// if need to force the entity filtering : Tag.findMeta({label: "Test"}, user, true);
let data = await Tag.findOneMeta({label: "Test"}, user);
let data = await Tag.findByIdMeta(id, user);

// Count - Promise
let data = await Tag.countMeta({label: "Test"});

// Create - Promise
let data = Tag.createMeta({label : "Test"}, user);

// Update - Promise
let data = await Tag.findOneAndUpdateMeta({_id: id}, {label: "Custom"}, user);
let data = await Tag.findByIdAndUpdateMeta(id, {label: "CustomFindByIdUpdate"}, user);
let data = await Tag.updateMeta({label: "CustomUpdate"}, user);

// Delete - Promise
let data = await Tag.findOneAndRemoveMeta({label: "Test2"}, user);
let data = await Tag.findOneAndRemoveMeta({label: "Test3"}, user);
let data = await Tag.findByIdAndRemoveMeta(id, user);
let data = await Tag.findByIdAndDeleteMeta(id, user);
let data = await Tag.deleteOneMeta({}, user);
let data = await Tag.deleteMeta({}, user);
// Set to true for soft deletion. It is possible to set to false if you want real deletion 


// Find deleted documents - Promise
let data = await Tag.findDeleted({label: "Test"}, user);
let data = await Tag.findOneDeleted({label: "Test2"}, user);
let data = await Tag.countDeleted({label: "Test"}, user);
````